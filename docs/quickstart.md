# EasyFront.js 使用文档
EasyFront.js是一款用于快速建立前端的小工具。
  
## 快速开始

### 如何使用

在您的html页面引入easyFront.js，请添加defer，需要确保文档已经加载完成才能进行处理。
```
<script defer src="easyFront.js"></script>
```
### 第一个例子

- 使用var标签定义一个双向绑定的变量，建议一个变量定义一次，不建议多个变量同时在一个var标签进行定义，这会导致响应式处理效率低下。
- 在任意标签中，不限于p标签，使用e-data属性将会自动的把变量插入到该元素当中，并且是响应式变化的。

```
<var name="title" value='Hello, World！'></var>
<p e-data="title"></p>
```

框架会自露出来EasyFront的对象，您可以控制台改变对象中的数据，查看网页响应式变化。

```
EasyFront.EFVal.data.title = 'i have a apple.'
```
## 功能介绍

### var标签
var标签是用来定义变量的，生成的变量统一由easyfront进行管理，你可以通过EasyFront.EFVal管理变。

[var标签的文档](./components/var.md)
