# var标签
var标签是用来定义变量的，生成的变量统一由easyfront进行管理，你可以通过EasyFront.EFVal管理变。

### 使用

```
<var name="title" value='Hello, World！'></var>
```

### Attributes

|  参数   | 说明  |  类型 |
|  ----  | ----  | ----  |
| name  | 变量名称 |string |
| value  | 变量的值，可以是数组、对象 |string |
