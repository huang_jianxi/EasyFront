import EFVal from './components/val'
import EFFor from './components/for'
import EFRequest from './components/request'
import EFShow from './components/show'
import EFOverrride from './components/val/override'
class EasyFront {
    EFVal: EFVal
    EFFor: EFFor
    EFRequest: EFRequest
    // EFOverrride: EFOverrride
    EFShow: EFShow
    constructor() {
        this.EFVal = new EFVal()
        this.EFFor = new EFFor(this)
        this.EFRequest = new EFRequest(this)
        // this.EFOverrride = new EFOverrride(this)
        this.EFShow = new EFShow(this)
    }
}
window['EasyFront'] = new EasyFront()