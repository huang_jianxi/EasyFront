class ESFor {
    easyFront: any = {}
    constructor(father) {
        this.easyFront = father
        console.log(this.easyFront)
        
        this,this.makeForTemplete()
    }
    makeForTemplete() {
        let forInfo = document.querySelectorAll("[e-for]")
        console.log(forInfo)
        this.ForTempleteReplace(forInfo)
    }
    ForTempleteReplace(forInfo) {
        console.log(forInfo)
        for (const dom of forInfo) {
            let valName = dom.getAttribute("e-for")
            let child = dom.children[0]
            this.doForTemplete(valName, child, dom)
            dom.removeChild(child)
            this.easyFront.EFVal.pushValChangeCallBack(`Efor_${valName}_push`, child ,(child, newVal)=>{
                this.makeOneForTemplete(newVal, child, valName, dom)
            })
            this.easyFront.EFVal.pushValChangeCallBack(valName, child ,(child, newVal)=>{
                dom.innerHTML = ""
                this.doForTemplete(valName, child, dom)
            })
        }
    }
    /**
     * 渲染数组前先把子项的回调事件删除
     * @date 2021-07-23
     * @param {string} valName
     * @returns {any}
     */
    cleanEforItemChangeCallBack(valName: string): any {
        let len = this.easyFront.EFVal._data[valName].length || 0
        console.log(len)
        this[`${valName}_index`] = 0
        for (let i = 0; i < len; i++) {
            delete this.easyFront.EFVal.modelValChangeCallBack[`Efor_${valName}_${i}`]
        }
    }
    doForTemplete(valName: string, child: any, dom: any) {
        this.cleanEforItemChangeCallBack(valName)
        for (const item of this.easyFront.EFVal._data[valName]) {
            this.makeOneForTemplete(item, child, valName, dom)
        }
    }
    makeOneForTemplete(item, child, valName, dom) {
        if(this[`${valName}_index`] == undefined) this[`${valName}_index`] = 0
        if(this[`${valName}_domMap`] == undefined) this[`${valName}_domMap`] = {}

        item.index = this[`${valName}_index`]++
        let new_child = child.cloneNode(true)
        this[`${valName}_domMap`][this[`${valName}_index`]] = new_child
        let vals = new_child.querySelectorAll("[e-for-data]")
        // 遍历复制节点
        for (const dom of vals) {
            let attribute = dom.getAttribute("e-for-data")
            dom.innerText = eval(`${attribute}`)
            if(attribute == 'item.index') {
                continue
            }
            this.easyFront.EFVal.pushValChangeCallBack(`Efor_${valName}_${item.index}`, new_child ,(new_child, newVal)=>{
                let vals = new_child.querySelectorAll("[e-for-data]")
                let item = newVal
                for (const dom of vals) {
                    let attribute = dom.getAttribute("e-for-data")
                    if(attribute == 'item.index') {
                        continue
                    }
                    dom.innerText = eval(`${attribute}`)
                }
            })
        }
        let clicks = new_child.querySelectorAll("[e-for-click]")
        for (const click of clicks) {
            click.addEventListener('click',() => {
                console.log(item)
                let fun = click.getAttribute("e-for-click")
                eval(`${fun}`)
             })
        }
        dom.appendChild(new_child)
    }
    /**
     * 因数组里单项无法监听
     * 所以通过set方法设置可以响应式改变
     * @date 2021-07-23
     * @param {string} key
     * @param {number} index
     * @returns {any}
     */
    set(valName: string, index: number, newVal: any): any {
        this.easyFront.EFVal.data[valName][index] = newVal
        this.easyFront.EFVal.modelValChangeCallBack[`Efor_${valName}_${index}`].forEach(element => {
            console.info("EasyFront[INFO]: 更新节点:" + element.data)
            element.ev(element.data, newVal)
        })
    }
    push(valName: string, newVal: any): any {
        this.easyFront.EFVal.data[valName].push(newVal)
        let cbs = this.easyFront.EFVal.modelValChangeCallBack[`Efor_${valName}_push`]
        for (const cb of cbs) {
            cb.ev(cb.data,newVal)
        }
    }
}
export default ESFor