class Override {
    easyFront: any = {}
    constructor(father) {
        this.easyFront = father
        this.arrayPush()
    }
    arrayPush() {
        let arrayPush = Array.prototype.push = (e) => {
            console.info("重写push方法，您要插入的是" + JSON.stringify(e))
            arrayPush[arrayPush.length] = e
            return arrayPush.length
        }
    }
}

export default Override