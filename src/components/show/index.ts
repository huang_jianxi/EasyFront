export default class eShowEngine {
    easyFront: any = {}
    constructor(father) {
        this.easyFront = father
        this.makeEShow()
    }
    makeEShow() {
        let shows = document.querySelectorAll("[e-show]") 
        for (const dom of shows) {
            let htmlDom = dom as HTMLElement
            let valName = dom.getAttribute("e-show")
            if(this.easyFront.EFVal._data[valName]) {
                htmlDom.style.display = "block"
            }else{
                htmlDom.style.display = "none"
            }
            this.easyFront.EFVal.pushValChangeCallBack(valName, htmlDom ,(dom, newVal)=>{
                if(newVal) {
                    dom.style.display = "block"
                }else{
                    dom.style.display = "none"
                }
            })
        }
    }
}